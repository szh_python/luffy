import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// const store = new Vuex.Store({
export default new Vuex.Store({
  state: {
    cart:{
      cart_length:0,
    }
  },
  mutations: {
    add_cart (state,val) { // 括号里面传值，第一个值一定要写上面传下来的state， 后面才是自己传来的值
      state.cart.cart_length = val;
    }
  }
})

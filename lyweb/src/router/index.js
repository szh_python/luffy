import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Nbcourse from '@/components/Nbcourse'
import CourseDetail from '@/components/CourseDetail'


import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      // name: 'HelloWorld',
      component: Home
    },
    {
      path: '/home',
      // name: 'HelloWorld',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/nbcourses/',
      name: 'Nbcourse',
      component: Nbcourse
    },
    {
      path: '/coursedetail/:id/',
      name: 'CourseDetail',
      component: CourseDetail
    }


  ]
})



// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import settings from './settings'
import '../static/css/reset.css'  //引入文件的写法, 引入全局css文件的写法
// import 'static/js/captcha'  // 使用别名static 可以直接指到static路径， 就跟@ 代表src一样， 系统有配置, 这里引入js可以不加.js, 加上也可以
import '../static/js/captcha' // 不屑后缀.js也是可以的， 使用绝对路径也可以， 上面的别名路径u也可以
import axios from 'axios'
require('video.js/dist/video-js.css');
require('vue-video-player/src/custom-theme.css');
import VideoPlayer from 'vue-video-player'
import Vuex from 'vuex'
import store from './store'  // store下创建的是index.js文件，引入可以不用写/index
Vue.use(Vuex)
Vue.use(VideoPlayer);


Vue.config.productionTip = false
Vue.prototype.$settings = settings// 加上这句话， 就可以在组件中通过this调用$settings拿到settings对象，再通过对象。host就拿到后台网址
Vue.prototype.$axios = axios
// 客户端配置是否允许ajax发送请求时附带cookie，false表示不允许
axios.defaults.withCredentials = false;




/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})

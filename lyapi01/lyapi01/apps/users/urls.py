from django.urls import path, re_path
from django.conf import settings
from . import views
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

# from users.views import LoginView

urlpatterns = [
    # path(r'login/', obtain_jwt_token),
    path(r'login/', views.LoginView.as_view()),
    # path(r'verify_token/', verify_jwt_token),
    path(r'verify_token/', refresh_jwt_token),# 校验加刷新

    path(r'check_phone/', views.CheckPhoneView.as_view()),
    path(r'register/', views.RegisterView.as_view()),

    path(r'get_sms_code/', views.SMScodeView.as_view()),

]

from django.db import models

# Create your models here.

# 让xadmin使用的用户和我们项目使用的用户是一套用户
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):

    wechat = models.CharField(max_length=32)
    phone = models.CharField(max_length=16)

    class Meta:
        db_table = 'ly_user'
        verbose_name = '用户信息'
        verbose_name_plural = verbose_name


















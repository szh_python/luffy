from django.contrib.auth.hashers import make_password
import re
from .utils import get_user_obj_by_account
from django.contrib.auth import authenticate, get_user_model
from django.utils.translation import ugettext as _
from rest_framework import serializers
# /home/szh/.virtualenvs/lyapi/lib/python3.6/site-packages/rest_framework_jwt/compat.py:6
# from .compat import Serializer
from rest_framework_jwt.compat import Serializer

from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.compat import get_username_field, PasswordField
from django_redis import get_redis_connection

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER



class CustomJSONWebTokenSerializer(Serializer):

    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(CustomJSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField()
        self.fields['password'] = PasswordField(write_only=True)
        self.fields['ticket'] = PasswordField(write_only=True)
        self.fields['randstr'] = PasswordField(write_only=True)

    @property
    def username_field(self):
        return get_username_field()



    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password'),
            'ticket': attrs.get('ticket'),
            'randstr': attrs.get('randstr'),
        }

        if all(credentials.values()):
            user = authenticate(self.context['request'],**credentials)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)

# register序列化器
from .models import User
class RegisterModelSerializer(serializers.ModelSerializer):
    #手机号， 密码， 验证码
    sms_code = serializers.CharField(max_length=6,min_length=4,write_only=True)
    # password = serializers.CharField(max_lenght=16)
    token = serializers.CharField(max_length=256,read_only=True)# 给token值，登录状态

    class Meta:
        model = User
        fields = ['id','username','phone','password','sms_code','token']
        extra_kwargs = {
            'password':{
                'write_only':True,
            },
            # 'sms_code':{
            #     'write_only':True,
            # },
            # 'token':{
            #     'read_only':True,
            # },
            'username':{
                'read_only':True,
            },
            'phone':{
              'write_only':True,
            },
            'id':{
                'read_only':True,
            }

        }

    # def validate_password(self,value):# 局部钩子
        # 可以对注册的密码做更严格的校验
        # return value

    def validate_phone(self,phone):
        # 手机号格式校验
        if not re.match('1\d{10}', phone):
            raise serializers.ValidationError('手机好格式有误')

        # 手机号唯一性校验
        ret = get_user_obj_by_account(phone)
        if ret:
            raise serializers.ValidationError('手机号已经被注册啦！！！')
        return phone

    # todo 验证sms_code  验证码
    def validate(self, data):# 全局钩子，trturn所有数据
        # 请求发送来的验证码
        sms_code = data.get('sms_code')
        phone = data.get('phone')

        # redis数据库中获取存的对应手机好的验证码
        conn = get_redis_connection('sms_code')
        redis_code = conn.get(f'sms_{phone}') #获取不到值返回None
        if redis_code:
            redis_code = redis_code.decode()
            if sms_code != redis_code:
                raise serializers.ValidationError('验证码输入有误！！！')

            return data

        else:
            raise serializers.ValidationError('验证码已经过期')





    def create(self,validated_data):
        password = validated_data.get('password')# 加密。 引入make_password
        # print('validated_data>>>',validated_data)
        hash_password = make_password(password)

        # 存储  #看源码的出的是自动用save保存的， 所以后面return user
        user = User.objects.create(**{
            'phone':validated_data.get('phone'),
            'password':hash_password,
            'username':validated_data.get('phone'),# 注册用户名默认给手机号

        })

        # jwt官网给的生成token值
        from rest_framework_jwt.settings import api_settings

        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token #接收 jwt提供的生成的token值， 给user对象赋值

        return user  # 模型类对象user.id user.username user.token  返回这3 个数据， 用户注册直接登录

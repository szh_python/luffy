from django.urls import path, re_path
from django.conf import settings
from . import views
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

# from users.views import LoginView

urlpatterns = [
    path(r'categorys/', views.CourseCategoryView.as_view()),
    path(r'list/', views.CourseListView.as_view()),
    re_path(r'detail/(?P<pk>\d+)/', views.CourseDetailListView.as_view()),  # /course/detail
    re_path(r'chapter/', views.ChapterListView.as_view()),  # /course/detail

]

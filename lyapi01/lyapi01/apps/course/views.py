from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView
from . import models
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.
from .serializers import CourseCategoryModelSerializer, CourseModelSerializer,CourseDetailModelSerializer,ChapterModelSerializer
from rest_framework.pagination import PageNumberPagination

class ChapterListView(ListAPIView):
    queryset = models.CourseChapter.objects.filter(is_show=True, is_deleted=False)
    serializer_class = ChapterModelSerializer
    filter_backends = [DjangoFilterBackend, ]
    filter_fields = ('course',)
    #  /course/chapter/?course=2


class CourseDetailListView(RetrieveAPIView):
    queryset = models.Course.objects.filter(is_show=True,is_deleted=False)
    serializer_class = CourseDetailModelSerializer


class CustomPagenation(PageNumberPagination):
    page_size = 5  # 每页显示5条记录
    page_query_param = 'page'  # ?page=2
    page_size_query_param = 'size'  # /list/?size=20, 允许前端自行控制显示
    max_page_size = 100  # 最大每页显示几条


class CourseCategoryView(ListAPIView):
    queryset = models.CourseCategory.objects.filter(is_show=True, is_deleted=False)
    serializer_class = CourseCategoryModelSerializer


class CourseListView(ListAPIView):
    queryset = models.Course.objects.filter(is_show=True, is_deleted=False)
    serializer_class = CourseModelSerializer
    filter_backends = [DjangoFilterBackend, ]
    filter_fields = ('course_category',)
    pagination_class = CustomPagenation

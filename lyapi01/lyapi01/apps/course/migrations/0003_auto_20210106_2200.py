# Generated by Django 2.2 on 2021-01-06 14:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0002_auto_20210106_2156'),
    ]

    operations = [
        migrations.RenameField(
            model_name='teacher',
            old_name='join_brief',
            new_name='brief',
        ),
    ]

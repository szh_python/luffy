from django.shortcuts import render
from course.models import Course
# Create your views here.
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import status
from django_redis import get_redis_connection
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class CartView(ViewSet):
    # authentication_classes =
    permission_classes = [IsAuthenticated, ]  #request.user  # 加上这句话， 表示添加够物车必须处于登录状态
    # 添加购物车
    def add(self,request):
        expire_id = 0  #表示永久有效
        # user_id:{course_id:expire, }a
        # user_id = 1
        user_id = request.user.id
        course_id = request.data.get('course_id')

        # 到底有没有这个课程
        try:
            course_obj = Course.objects.get(id=course_id)
        except:
            # return Response({'error':'该课程不存在'}, status=400)
            return Response({'error':'该课程不存在'}, status=status.HTTP_400_BAD_REQUEST)

        # 保存购物车数据
        conn = get_redis_connection('cart')
        # 添加购物车之前，先判断一下课程是否已经在购物车中
        ret = conn.hget(user_id,course_id)
        if ret:
            cart_len = conn.hlen(user_id)
            return Response({'error': '该课程已在购物车中','cart_len':cart_len}, status=status.HTTP_400_BAD_REQUEST)

        conn.hset(user_id, course_id, expire_id)
        cart_len = conn.hlen(user_id)

        return Response({'msg': '购物车添加成功','cart_len':cart_len})
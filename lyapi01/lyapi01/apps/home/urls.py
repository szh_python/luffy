from django.urls import path, re_path
from django.conf import settings
from . import views



urlpatterns = [
    path(r'banner/',views.BannerView.as_view()),
    path(r'nav/top/', views.NavTopView.as_view()),

]

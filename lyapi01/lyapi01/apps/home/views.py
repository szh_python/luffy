from django.shortcuts import render
# Create your views here.
from rest_framework.generics import ListAPIView
from . import models
from .serializer import BannerModelSerializer,NavModelSerializer
from lyapi01.settings import contains


class BannerView(ListAPIView):

    queryset = models.Banner.objects.filter(is_show=True,is_deleted=False).order_by('orders')[0:contains.BANNER_LENGTH]
    serializer_class = BannerModelSerializer


class NavTopView(ListAPIView):
    queryset = models.Nav.objects.filter(is_show=True,is_deleted=False,position=1).order_by('orders')[0:contains.NAV_TOP_LENGTH]
    serializer_class = NavModelSerializer


import json
import logging
logger = logging.getLogger('django')

from ronglian_sms_sdk import SmsSDK

from django.conf import settings
from lyapi01.settings import contains
def send_message(code,mobile):
    accId = settings.SMS_SETTINGS.get('accId')
    accToken = settings.SMS_SETTINGS.get('accToken')
    appId = settings.SMS_SETTINGS.get('appId')

    sdk = SmsSDK(accId, accToken, appId)
    # tid = '容联云通讯平台创建的模板'
    # tid = '1'  测试的短信模板id为1

    tid = settings.SMS_SETTINGS.get('tid')
    # mobile = '手机号1,手机号2'
    # mobile = '13185090519'
    # datas = ('1234', contains.SMS_EXPIRE_TIME)  # 验证码  间隔时间
    datas = (code, contains.SMS_EXPIRE_TIME)  # 验证码  间隔时间
    resp = sdk.sendMessage(tid, mobile, datas)
    # {"statusCode":"000000","templateSMS":{"smsMessageSid":"92ae3bbd4d474e25892239cee652c4f7","dateCreated":"20201228085954"}}
    # print(resp)

    ret = json.loads(resp)
    status_code = ret.get('statusCode')
    if status_code == "000000":

        return True
    else:
        logger.error('%s 手机号，短信发送有误, 错误码为 %s' % (mobile, status_code))
        return False
    # return  ret.get('statusCode')

# if __name__ == '__main__':
    # send_message()
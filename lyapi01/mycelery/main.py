
from celery import Celery

import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'lyapi01.settings.dev')

import django
django.setup()

app = Celery('lyapi01')

app.config_from_object('mycelery.config')

app.autodiscover_tasks(['mycelery.sms', 'mycelery.email'])

# 注意：运行celery启动指令时，要在mycelery外层来执行。

